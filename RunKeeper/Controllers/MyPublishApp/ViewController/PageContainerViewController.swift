import UIKit

class PageContainerViewController: UIViewController {

    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
      
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
          Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: " Developer Apps")
    //    self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Developer Apps"
        setNavigationBarBackButton()
    }
    
    @IBAction func onButtonNextTap(_ sender: Any) {
        
    }
    
    
    func setNavigationBarBackButton() {


         self.navigationItem.setHidesBackButton(true, animated:false)

         //your custom view for back image with custom size
         let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
         let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))

         if let imgBackArrow = UIImage(named: "back-arrow-png") {
             imageView.image = imgBackArrow
         }
         view.addSubview(imageView)

         let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
         view.addGestureRecognizer(backTap)

         let leftBarButtonItem = UIBarButtonItem(customView: view)
         self.navigationItem.leftBarButtonItem = leftBarButtonItem
         
         
         }
    

    @objc func backToMainViewController() {
           
            let transition = CATransition()
            transition.duration = 0.4
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
    
               
        }
}

