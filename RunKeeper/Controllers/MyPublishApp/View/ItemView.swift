import Foundation
import UIKit


@IBDesignable
class ItemView: UIView {
    static let CAROUSEL_ITEM_NIB = "CarouselItem"
      var appStoreLink  = URL.init(string: "")
    
    @IBOutlet var vwContent: UIView!
    @IBOutlet var vwBackground: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet weak var appIconImgView: UIImageView!

    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initWithNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initWithNib()
    }
    
    convenience init(titleText: String? = "", background: UIColor? = .red, appIconName:String,appStoreLink:String) {
        self.init()
        lblTitle.text = titleText
        appIconImgView.image = UIImage.init(named:appIconName)
        appIconImgView.layer.cornerRadius = 25;
        self.appStoreLink = URL.init(string: appStoreLink)
        vwBackground.backgroundColor = background
        appIconImgView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(appIconImgViewClickAction(tapGesture:)))
        appIconImgView.addGestureRecognizer(tapGesture)
    }
    
    fileprivate func initWithNib() {
        Bundle.main.loadNibNamed(ItemView.CAROUSEL_ITEM_NIB, owner: self, options: nil)
        vwContent.frame = bounds
        vwContent.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(vwContent)
    }
    
    @objc func appIconImgViewClickAction (tapGesture:UITapGestureRecognizer) {
    
                   if #available(iOS 10.0, *) {
                       UIApplication.shared.open(self.appStoreLink!, options: [:], completionHandler: nil)
                   } else {
                       UIApplication.shared.openURL(self.appStoreLink!)
                   }
        
    }
}
