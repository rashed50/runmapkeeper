//
//  ViewController.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 28/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import UIKit
import SideMenu
import GRDB

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if(!Helper.isProfileDataExist()){
            goToPorfileView()
        }
        DBHelper.CreateDatabase()
        
        
       
    }
    
    @IBAction func startNewRunAction(_ sender: Any) {
        
        startNewRunView()
    }
    
    func startNewRunView(){
           
           let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "newRunViewController") as! NewRunViewController
           self.navigationController?.pushViewController(detailsView, animated: true)
       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper.setNavigationBarProperty(navbar: self.navigationController, size: 18, title: "Run Map Keeper")
    }
    
    func goToPorfileView() {
        let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
                    self.navigationController?.pushViewController(detailsView, animated: true)
    }
}

extension MainViewController: SideMenuNavigationControllerDelegate {

    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }

    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }

    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }

    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
    
    
}


