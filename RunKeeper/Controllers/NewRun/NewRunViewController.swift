/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import CoreLocation
import MapKit
import CoreMotion
import HealthKit


class NewRunViewController: UIViewController {
    
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var avgPaceLabel: UILabel!
    
    @IBOutlet weak var calorieLabel: UILabel!
    @IBOutlet weak var calorieValueLabel: UILabel!
    
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var runInfoView: UIView!
    
    
    private var run: Run?
    private let locationManager = LocationManager.shared
    private var seconds = 0
    private var timer: Timer?
    private var distance = Measurement(value: 0, unit: UnitLength.meters)
    private var locationList: [CLLocation] = []
    private var calorieBurned : Float = 0
    private var userActivityTypeId : Int8 = 1
    private var numberOfSteps  : Int64 = 0
    
    var userHealthProfile = UserHealthProfile.init()
    let activityManager = CMMotionActivityManager()
    let pedoMeter = CMPedometer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        loadUpdateHealthInfo()
        settAppBackgroundRunningOrNot()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarBackButton()
        Helper.setNavigationBarProperty(navbar: self.navigationController, size: 18, title: "Run Details")
        self.title  = "Run Details"
        if(Helper.getAutoStartRunPlay()){
            startTapped()
        }
        if(Helper.getShowCalories())
        {
            calorieLabel.isHidden = false
            calorieValueLabel.isHidden = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        locationManager.stopUpdatingLocation()
    }
    
    
    
    
    
    //
    // MARK : USER INTERFACE ACTION
    //
    
    
    @IBAction func scrollContentTapped(_ sender: Any) {
        
        
        pageControll.currentPage = pageControll.currentPage > 0 ? 0 : 1
        
        if(pageControll.currentPage == 0){
            runInfoView.isHidden = false
            mapContainerView.isHidden = true
            self.title  = "Run Details "
            
        }else if(pageControll.currentPage == 1){
            runInfoView.isHidden = true
            mapContainerView.isHidden = false
            self.title  = "Run Detail in Map"
        }
    }
    
    
    
    @IBAction func startTapped() {
        mapView.removeOverlays(mapView.overlays)
        startRun()
        seconds = 0
        distance = Measurement(value: 0, unit: UnitLength.meters)
        locationList.removeAll()
        updateDisplay()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            self.eachSecond()
        }
        startLocationUpdates()
        
    }
    
    @IBAction func stopTapped() {
        let alertController = UIAlertController(title: "End run?",
                                                message: "Do you wish to end your run?",
                                                preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(UIAlertAction(title: "Save", style: .default) { _ in
            self.stopRun()
            self.locationManager.stopUpdatingLocation()
            self.saveRun()
            self.showDetailsView()
        })
        alertController.addAction(UIAlertAction(title: "Discard", style: .destructive) { _ in
            self.stopRun()
            self.locationManager.stopUpdatingLocation()
            _ = self.navigationController?.popToRootViewController(animated: true)
        })
        
        
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view //to set the source of your alert
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
        }
        
        present(alertController, animated: true)
    }
    
    
    
    
    
    //
    // MARK: USER DEFINE METHODS
    //
    
    func setNavigationBarBackButton() {
        
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
        
        if let imgBackArrow = UIImage(named: "back-arrow-png") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController(tap:)))
        view.addGestureRecognizer(backTap)
        
        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        
    }
    
    @objc func backToMainViewController(tap : UITapGestureRecognizer) {
        guard let _ = timer?.isValid
            else {
                self.backToView()
                return
        }
        
        let alert = UIAlertController.init(title: "Warning", message: "Do You Want to Save you activity ?", preferredStyle: .alert)
        let okAcction = UIAlertAction(title: "OK", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.saveRun()
            self.backToView()
        })
        let  noAction = UIAlertAction(title: "No", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.backToView()
        })
        
        alert.addAction(okAcction)
        alert.addAction(noAction)
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view //to set the source of your alert
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
        }
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func backToView() {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
    private func startRun() {
        //  launchPromptStackView.isHidden = true
        // dataStackView.isHidden = false
        startButton.isHidden = true
        stopButton.isHidden = false
        initializeMotionActivity()
        
    }
    
    
    private func stopRun() {
        // launchPromptStackView.isHidden = false
        //  dataStackView.isHidden = true
        startButton.isHidden = false
        stopButton.isHidden = true
    }
    
    func showDetailsView() {
        
        let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "runDetailsViewController") as! RunDetailsViewController
        detailsView.run = run
        detailsView.previousController = "newrun"
        
        self.navigationController?.pushViewController(detailsView, animated: true)
    }
    func eachSecond() {
        seconds += 1
        updateDisplay()
    }
    
    private func updateDisplay() {
       
        let formattedDistance = FormatDisplay.distance(distance)
        let formattedTime = FormatDisplay.time(seconds)
         /*
        let formattedPace = FormatDisplay.pace(distance: distance,
                                               seconds: seconds,
                                               outputUnit: UnitSpeed.minutesPerKilometer)
        let formattedSpeed = FormatDisplay.speed(distance: distance,
        seconds: seconds,
        outputUnit: UnitSpeed.kilometerPerMinute)
        */
        distanceLabel.text = String(describing: formattedDistance.split(separator: " ").first!)
        timeLabel.text = "\(formattedTime)"

        
        paceLabel.text = String.init(format: "%.2f min/km",((Double(seconds) / 60) / (distance.value / 1000)))
        avgPaceLabel.text = String.init(format: "%.2f km/min",((distance.value / 1000)/(Double(seconds) / 60)))
        
        let height = Float(userHealthProfile.heightInMeters)
        let weight = Float(userHealthProfile.weightInKilograms)
        
        if(distance.value * 1000 > 0 ){
            let weightMul = 0.035 * weight
            var speedInMeter = (Float) ((distance.value) / Double(seconds))
            speedInMeter = speedInMeter * speedInMeter
            let divVal = speedInMeter / height
            calorieBurned = ( weightMul + (divVal * 0.0029 * 60))
          
            
            let calorieBurnedd =   Helper.calculateEnergyExpenditureWith(height: Float(userHealthProfile.heightInMeters*100), ageCalculated: Float(Helper.getAgeFromDOB(date: userHealthProfile.dob!)), weight: Float(userHealthProfile.weightInKilograms), gender: userHealthProfile.sex!, durationInSeconds: seconds, stepsTaken: seconds, strideLength: 123, kmTravelled: Float(distance.value))
            print("CalorieBurned  %@",calorieBurnedd)
            calorieValueLabel.text = String.init(format: "%.f",calorieBurnedd)
            
            
        }
        
    }
    
    private func startLocationUpdates() {
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.distanceFilter = 10
        locationManager.startUpdatingLocation()
    }
    
    
    private func saveRun() {
        
        print(locationList)
        var loclist  = [Dictionary<String,Double>]()
        for location in locationList {
            var dic = [String:Double]()
            dic  = ["lat":location.coordinate.latitude,"lon":location.coordinate.longitude,"timestamp":Double(Date().secondsSince1970)]
            print(dic)
            loclist.append(dic)
        }
        
        var myJsonString = ""
        do {
            let data =  try JSONSerialization.data(withJSONObject:loclist, options: .prettyPrinted)
            myJsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
            print(myJsonString)
        } catch {
            print(error.localizedDescription)
        }
        
        let rulocationList = Helper.jsontStringToRunLocationList(locString: myJsonString)
        
        run = Run.init(id : 0, distance: distance.value, time:Date().secondsSince1970, duration:seconds, calorie: calorieBurned, runLoc:rulocationList,activityType:userActivityTypeId,stepCount: numberOfSteps)
        DBHelper.insetRunInformation(run: run! ,locationString: myJsonString)
        timer?.invalidate()
    }
    
    
    
    
    //
    // MARK : PEDO MENTER ACTION
    //
    
    
    func initializeMotionActivity() {
        
        
        
        if(CMMotionActivityManager.isActivityAvailable()){
            self.activityManager.startActivityUpdates(to: OperationQueue.main, withHandler: { (data: CMMotionActivity!) -> Void in
                if(data.stationary == true){
                    print("stationary true")
                } else if (data.walking == true){
                     self.userActivityTypeId = 1
                    
                } else if (data.running == true){
                    self.userActivityTypeId = 2
                    
                } else if (data.automotive == true){
                     self.userActivityTypeId = 3
                    
                }
                
            })
            
        }
        
        
        self.pedoMeter.startUpdates(from: NSDate() as Date) { (data: CMPedometerData!, error) -> Void in
            
            if(error == nil){
                print("\(data.numberOfSteps)")
                //  self.calorieValueLabel.text = ("\(data.numberOfSteps) Steps")
                self.numberOfSteps = Int64(truncating: data.numberOfSteps)
            }
            
        }
        
    }
    
    
    
    private func loadUpdateHealthInfo() {
        
        userHealthProfile = Helper.getUserProfileInformation()
    }
    
    
    func settAppBackgroundRunningOrNot(){
        
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
}


extension NewRunViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer(overlay: overlay)
        }
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = Helper.getHexaStringToColor(hexa:"7199EC")
        renderer.lineWidth = 3
        return renderer
    }
}


extension NewRunViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for newLocation in locations {
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
            
            if let lastLocation = locationList.last {
                let delta = newLocation.distance(from: lastLocation)
                distance = distance + Measurement(value: delta, unit: UnitLength.meters)
                
                let coordinates = [lastLocation.coordinate, newLocation.coordinate]
                mapView.addOverlay(MKPolyline(coordinates: coordinates, count: 2))
                let region = MKCoordinateRegion(center: newLocation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
                mapView.setRegion(region, animated: true)
            }
            
            locationList.append(newLocation)
        }
    }
}
