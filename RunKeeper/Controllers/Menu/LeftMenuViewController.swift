//
//  LeftMenuViewController.swift
//  SlideMenu
//
//  Created by Rashedul Hoque on 19/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import UIKit
import MessageUI


class LeftMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    // Scan Analog File
    let menuList = [Helper.getUserProfileName(),"Profile","New Run","Prancercise", "Past Activities", "Developer App","Setting"]
    let cellReuseIdentifier = "cell"
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        tableView.register(UINib(nibName: "MenuTableCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        tableView.separatorStyle = .none;
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return menuList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
        var celll: MenuTableCell! = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? MenuTableCell
        if celll == nil {
            celll = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? MenuTableCell
        }
        celll.menuTitleLabel!.text = menuList[indexPath.row]
        celll.menuImageView!.image = UIImage.init(named: String.init(format: "menu_%d", indexPath.row))
        celll.menuImageView.contentMode = .scaleAspectFit
        if(indexPath.row == 0 ){
            var frm = celll.menuImageView.frame
            frm.size.width = 0
            celll.menuImageView.frame = frm
      
        }
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.black //   Helper.getAppColor()
        celll.selectedBackgroundView = backgroundView
        
        // self.tableView.separatorColor = UIColor.gray
       // self.tableView.separatorStyle = .singleLine;
        
        return celll
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0){
            
        }
        else if(indexPath.row == 1){
             
            showMyProfileView()
            }
        else if(indexPath.row == 2){
            showNewRunView()
        }
        else if(indexPath.row == 3){
                showPrancerciseWorkOutView()
        }
        else if(indexPath.row == 4){
            showHistoryView()
        }
        else if(indexPath.row == 6){
           showAppSettingView()
        }
        else if(indexPath.row == 5){
           showDeveloperApps()
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
         
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 120))
        
        /*
        let label = UILabel()
        label.frame = CGRect.init(x: headerView.frame.width/2-60, y: headerView.frame.height/2-60, width: 120, height: 120)
        label.text = Helper.getUserProfileName()
        label.textAlignment = .center
        label.numberOfLines = 2
        label.minimumScaleFactor = 0.5
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = UIColor.white // Helper.getAppColor()
        label.layer.cornerRadius = label.frame.width/2
        label.layer.masksToBounds = true
         label.textColor = UIColor.red // my custom colour
        headerView.addSubview(label)
        */
        
        let imgView = UIImageView()
              imgView.frame = CGRect.init(x: headerView.frame.width/2-60, y: headerView.frame.height/2-60, width: 120, height: 120)

              imgView.backgroundColor = UIColor.white // Helper.getAppColor()
        imgView.contentMode = .scaleAspectFit
              imgView.layer.cornerRadius = imgView.frame.width/2
              imgView.layer.masksToBounds = true
        imgView.image = UIImage.init(named: "running")
        headerView.addSubview(imgView)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func showNewRunView(){
        
        let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "newRunViewController") as! NewRunViewController
        self.navigationController?.pushViewController(detailsView, animated: true)
    }
    func showPrancerciseView() {
        
       // let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pranProfileViewController") as! PranProfileViewController
        //      self.navigationController?.pushViewController(detailsView, animated: true)
    }
    func showPrancerciseWorkOutView() {
        
        let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "workoutsTableViewController") as! WorkoutsTableViewController
              self.navigationController?.pushViewController(detailsView, animated: true)
    }
    func showHistoryView() {
        
        
        let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "historyViewController") as! HistoryViewController
        self.navigationController?.pushViewController(detailsView, animated: true)
    }
    func showAppSettingView() {
             
           let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settingViewController") as! SettingViewController
           self.navigationController?.pushViewController(detailsView, animated: true)
       }
    func showMyProfileView() {
               
             let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
             self.navigationController?.pushViewController(detailsView, animated: true)
         }
    func showDeveloperApps() {
        
        let story = UIStoryboard.init(name: "MyAppMain", bundle: nil)
        var viewCon : UIViewController
        viewCon = story.instantiateViewController(withIdentifier: "pageContainerViewController")
        self.navigationController?.pushViewController(viewCon, animated: true)
         
    }
    
}




extension LeftMenuViewController : MFMailComposeViewControllerDelegate{
    
    public func mailComposeController(_ controller: MFMailComposeViewController,
                                      didFinishWith result: MFMailComposeResult,
                                      error: Error?) {
        switch (result) {
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
        case .sent:
            controller.dismiss(animated: true, completion: nil)
        case .failed:
            controller.dismiss(animated: true, completion: {
                
                let sendMailErrorAlert = UIAlertController.init(title: NSLocalizedString("Error", comment: ""),
                                                                message: "Unable to send email. Please check your email and Internet Connection " +
                    "settings and try again.", preferredStyle: .alert)
                sendMailErrorAlert.addAction(UIAlertAction.init(title: NSLocalizedString("OK", comment: ""),
                                                                style: .default, handler: nil))
                controller.present(sendMailErrorAlert, animated: true, completion: nil)
            })
        default:
            break;
        }
    }
    
}
