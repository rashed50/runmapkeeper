//
//  HistoryViewController.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 7/7/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
     var runHistoryList = [Run]()
     var workoutHistoryList = [Prancercise]()
     let cellReuseIdentifier = "cell"
    var currentLoadedDataType : Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        reloadDataFromDb(type: 1)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBarBackButton()
        Helper.setNavigationBarProperty(navbar: self.navigationController, size: 18, title: "Past Activities")
        self.title = "Past Activities"
    }
    
    //  MARK:- USER UI ACTION
    
    @IBAction func searchDataInfo(_ sender: Any) {
        
        openDataTypeActionsheet()
        
    }
    
    
    func reloadDataFromDb(type: Int) {
        if type == 1 {
             currentLoadedDataType = 1
             runHistoryList = DBHelper.getAllRunInformation()
             print(runHistoryList.count)
        }else if type == 2{
            currentLoadedDataType = 2
            workoutHistoryList = DBHelper.getAllPrancerciseWorkoutInformation()
            print(workoutHistoryList.count)
        }
        tableView.reloadData()
    }
    
    func openDataTypeActionsheet() {
        let alert = UIAlertController.init(title: "Data Type", message: "", preferredStyle: .actionSheet)
        let runDataAction = UIAlertAction.init(title: "Run Data", style: .default, handler: { alrt in
            self.reloadDataFromDb(type: 1)
        })
        let workoutActin = UIAlertAction.init(title: "Workout Data", style: .default, handler: {action in
            
            self.reloadDataFromDb(type: 2)
        })
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: {action in
            
            
        })
        alert.addAction(runDataAction)
        alert.addAction(workoutActin)
        alert.addAction(cancelAction)
        
 
        if let popoverController = alert.popoverPresentationController {
                              popoverController.sourceView = self.view //to set the source of your alert
                              popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                              popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                          }
        
        present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentLoadedDataType == 1{
            return runHistoryList.count}
        else if currentLoadedDataType == 2{
            return workoutHistoryList.count
        }
         return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
        var celll: HistoryTableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? HistoryTableViewCell
        if celll == nil {
            celll = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? HistoryTableViewCell
        }
        
        celll.iconImageView!.image = UIImage.init(named: "running")
        celll.iconImageView.contentMode = .scaleAspectFit
        
        if currentLoadedDataType == 1 {
          
        let aRun = runHistoryList[indexPath.row]
        celll.ditanceLabel.text = String.init(format: "Distance: %.2f km ", aRun.distance/1000)
        celll.durationLabel.text = "Time: \(FormatDisplay.time(aRun.duration))"
        celll.durationLabel.minimumScaleFactor = 0.5
            if indexPath.row == 0{
                celll.runDateLabel.text = String.init(format:"Date: %@,          Calorie: %.2f cal", Helper.convertTimeStampToDateAsString(arun: aRun.runDate),aRun.calorie + 0.5)
      
     }
     else if indexPath.row == 1{
         celll.runDateLabel.text = String.init(format:"Date: %@,          Calorie: %.2f cal", Helper.convertTimeStampToDateAsString(arun: aRun.runDate),aRun.calorie + 0.7 )
     } else if indexPath.row == 2{
                celll.runDateLabel.text = String.init(format:"Date: %@,          Calorie: %.2f cal", Helper.convertTimeStampToDateAsString(arun: aRun.runDate),aRun.calorie - 0.7 )
                }
                  else if indexPath.row == 3{
                      celll.runDateLabel.text = String.init(format:"Date: %@,          Calorie: %.2f cal", Helper.convertTimeStampToDateAsString(arun: aRun.runDate),aRun.calorie + 6)
                  }
            
        }else if currentLoadedDataType == 2{
            
            let aRun = workoutHistoryList[indexPath.row]
            celll.ditanceLabel.text = String.init(format: "Carlorie: %.2f cal", aRun.colorie)
            celll.durationLabel.text = "Time: \(FormatDisplay.time(Int(aRun.duration)))"
            celll.durationLabel.minimumScaleFactor = 0.5
            celll.runDateLabel.text = String.init(format:"Date: %@", Helper.convertTimeStampToDateAsString(arun:aRun.timeStamp))
  
            
            
        }
        return celll
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if currentLoadedDataType == 1{
            showDetailsView(run:runHistoryList[indexPath.row])
            
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func showDetailsView(run: Run) {
        
        
        let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "runDetailsViewController") as! RunDetailsViewController
        detailsView.run = run
        detailsView.previousController = "history"
     
        self.navigationController?.pushViewController(detailsView, animated: true)
    }

    
   func setNavigationBarBackButton() {


              self.navigationItem.setHidesBackButton(true, animated:false)

              //your custom view for back image with custom size
              let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
              let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))

              if let imgBackArrow = UIImage(named: "back-arrow-png") {
                  imageView.image = imgBackArrow
              }
              view.addSubview(imageView)

              let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
              view.addGestureRecognizer(backTap)

              let leftBarButtonItem = UIBarButtonItem(customView: view)
              self.navigationItem.leftBarButtonItem = leftBarButtonItem
              
              
              }
         

         @objc func backToMainViewController() {
                
                 let transition = CATransition()
                 transition.duration = 0.4
                 transition.type = CATransitionType.push
                 transition.subtype = CATransitionSubtype.fromLeft
                 self.navigationController?.view.layer.add(transition, forKey: kCATransition)
                 self.navigationController?.popViewController(animated: false)
         
                    
             }
}
