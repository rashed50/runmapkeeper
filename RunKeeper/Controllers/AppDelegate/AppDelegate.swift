//
//  AppDelegate.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 28/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

     var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let locationManager = LocationManager.shared
        locationManager.requestWhenInUseAuthorization()
        
        
        return true
    }

   

}

