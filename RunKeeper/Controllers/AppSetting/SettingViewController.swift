//
//  ttbleViewController.swift
//  kidsEducation
//
//  Created by Rashedul Hoque on 5/6/20.
//  Copyright © 2020 education. All rights reserved.
//

import UIKit
import MessageUI
//import GoogleMobileAds


class SettingViewController: UITableViewController {

    
    @IBOutlet weak var adView: UIView!
    @IBOutlet weak var studyAutoPlaySwitch1: UISwitch!
    @IBOutlet weak var autoSaveDrawingImage: UISwitch!
    
    @IBAction func studyAutoPlayAction(_ sender: Any) {
        let st = sender as! UISwitch
        Helper.setAutoStartRunPlay(onOff: st.isOn)
    }
    
    @IBAction func autoSaveDrawingImageAction(_ sender: Any) {
        let st = sender as! UISwitch
        Helper.setShowCalories(onOff: st.isOn)
    }
    
    @IBAction func feedbackAndSuggestionAction(_ sender: Any) {
         
         Helper.sendEmail(viewCon: self, fileUrl:nil ,operationType: 1)
    }
    
    
    @IBAction func tellFriendAction(_ sender: Any) {

           Helper.sendEmail(viewCon: self, fileUrl:nil ,operationType: 2)
       
    }
    
    @IBAction func developerAppsAction(_ sender: Any) {
        
        let story = UIStoryboard.init(name: "MyAppMain", bundle: nil)
        var viewCon : UIViewController
        if #available(iOS 13.0, *) {
              viewCon = story.instantiateViewController(identifier: "pageContainerViewController")
        } else {
           viewCon = story.instantiateViewController(withIdentifier: "pageContainerViewController")
        }
        
        self.navigationController?.pushViewController(viewCon, animated: true)
         
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        Helper.setNavigationBarProperty(navbar: self.navigationController, size: 18, title: "App Setting")
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
          Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:" Setting  ")
         setNavigationBarBackButton()
        autoSaveDrawingImage.isOn = Helper.getAutoStartRunPlay()
        studyAutoPlaySwitch1.isOn = Helper.getShowCalories()
          
      }

    
    func setNavigationBarBackButton() {

        self.navigationItem.setHidesBackButton(true, animated:false)

        //your custom view for back image with custom size
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))

        if let imgBackArrow = UIImage(named: "back-arrow-png") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)

        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController(tap:)))
        view.addGestureRecognizer(backTap)
        
        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        
        }

    @objc func backToMainViewController(tap : UITapGestureRecognizer) {
           
            let transition = CATransition()
            transition.duration = 0.4
            transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
    
               
        }
     
       
}


extension SettingViewController : MFMailComposeViewControllerDelegate{
   
     public func mailComposeController(_ controller: MFMailComposeViewController,
                                      didFinishWith result: MFMailComposeResult,
                                      error: Error?) {
        switch (result) {
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
        case .sent:
            controller.dismiss(animated: true, completion: nil)
        case .failed:
            controller.dismiss(animated: true, completion: {
               
                let sendMailErrorAlert = UIAlertController.init(title: "Failed",
                                                                message: "Unable to send email. Please check your email and Internet Connection " +
                    "settings and try again.", preferredStyle: .alert)
                sendMailErrorAlert.addAction(UIAlertAction.init(title: "OK",
                                                                style: .default, handler: nil))
                controller.present(sendMailErrorAlert, animated: true, completion: nil)
            })
        default:
            break;
        }
     }

}
