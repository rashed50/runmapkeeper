//
//  ProfileTableCell.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 17/7/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import UIKit

class ProfileTableCell: UITableViewCell {

    
    @IBOutlet weak var infoTitleLabel: UILabel!
    @IBOutlet weak var infoValueButton : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
