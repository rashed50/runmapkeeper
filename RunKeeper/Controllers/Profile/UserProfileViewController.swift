//
//  ViewController.swift
//  ExpandCollapseSections
//
//  Created by ProgrammingWithSwift on 2019/12/23.
//  Copyright © 2019 ProgrammingWithSwift. All rights reserved.
//

import UIKit



class UserProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UIPickerViewDelegate, UIPickerViewDataSource {
     
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var myPickerView: UIPickerView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var pickerViewContainer: UIView!
    
    var  selectedRow : Int = -1
    var  selectedSection : Int = -1
    var keyboardTypeingText : String = ""
    let tableViewData = [[],[],[]]
    var pickerData  = [String]()
    
    var tableViewDataDic : NSMutableDictionary = [
        "Alarmer" : [Dictionary<String, String>](),
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationController?.navigationBar.isHidden = false
        self.setNavigationBarBackButton()
        self.showHidePickerViewContainer(isHidden: true)
        tableViewDataDic = Helper.getUserProfileData()
 

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false;
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title: "Profile")
        
    }
    
    
    
    //
    // TABLE VIEW DELEGATE METHOD
    //
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewDataDic.allKeys.count //self.tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let  allSecArray = tableViewDataDic.allKeys as NSArray
        let sectionTitle = allSecArray.object(at: section) as! String
        let sectionArray = tableViewDataDic.object(forKey: String(sectionTitle)) as! NSArray
        return  sectionArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ProfileTableCell") as! ProfileTableCell
        
        let  allSecArray = tableViewDataDic.allKeys as NSArray
        let sectionTitle = allSecArray.object(at: indexPath.section) as! String
        let sectionArray = tableViewDataDic.object(forKey: String(sectionTitle)) as! NSArray
        let info = (sectionArray[indexPath.row] as! Dictionary<String, String>)
        
        cell.infoTitleLabel.text = info["title"]
        cell.infoValueButton.setTitle(info["value"], for: .normal)
        cell.infoValueButton.tag = (indexPath.section*10)+indexPath.row
        cell.infoValueButton.addTarget(self, action: #selector(cellButtonAsLabelClickAction), for: .touchUpInside)
        cell.infoValueButton.addTarget(cell.infoValueButton, action: #selector(becomeFirstResponder), for: .touchUpInside)
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    private func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
 
    
    
    //
    // MARK USER DEFINE METHOD
    //
    
    func setNavigationBarBackButton() {
        
        self.navigationItem.setHidesBackButton(true, animated:false)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
        if let imgBackArrow = UIImage(named: "back-arrow-png") {
            imageView.image = imgBackArrow
        }
        view.addSubview(imageView)
        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
        view.addGestureRecognizer(backTap)
        let leftBarButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc func backToMainViewController() {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
        
    }
    
 
    
    
    @objc func cellButtonAsLabelClickAction(sender: UIButton!) {
        print("Button tapped")
        let section = sender.tag/10
        let row = sender.tag%10
        
     
        if(section == 0 && (row == 1 || row == 2)){
            self.resignFirstResponder()
            loadPickerView(rowNumber: row, sectionNumber: section)
        }else{
            
            showHidePickerViewContainer(isHidden: true)
            keyboardTypeingText = ""
            selectedRow = row;
            selectedSection = section
            let indexPath = IndexPath(row: selectedRow, section:selectedSection)
            let tableViewCell = tableView.cellForRow(at: indexPath) as! ProfileTableCell
            self.inputView = tableViewCell.infoValueButton.inputView
            
            self.becomeFirstResponder()
        }
    }
    
    
    //
    //  PICKER VIEW  METHOD
    //
    
    func loadPickerView(rowNumber: Int,sectionNumber:Int) {
         
        if(sectionNumber == 0){
            if( rowNumber == 1){
                
                datePicker.tag = sectionNumber*10 + rowNumber
                datePicker.datePickerMode = .date
                datePicker.isHidden = false
                self.myPickerView.isHidden = true
                
            }else if(rowNumber == 2){
                datePicker.isHidden = true
                self.myPickerView.isHidden = false
                
                self.myPickerView.delegate = self
                self.myPickerView.dataSource = self
                self.myPickerView.tag = sectionNumber*10 + rowNumber
                pickerData = ["Male","Female"]
                
            }
            showHidePickerViewContainer(isHidden: false)
            self.myPickerView.reloadAllComponents()
        }
         
        
        
        
    }
    
    func showHidePickerViewContainer(isHidden:Bool) {
        
        self.pickerViewContainer.frame.size.width = UIScreen.main.bounds.size.width
        UIView.animate(withDuration: 0.01, animations: {
            if(isHidden)
            {
                
                self.pickerViewContainer.frame.origin.y = 1500
            } else {
                
                self.pickerViewContainer.frame.origin.y = self.view.frame.size.height - 300
            }
            self.pickerViewContainer.layoutIfNeeded()
        }) { (finished) in
            UIView.animate(withDuration: 0.1, animations: {})
        }
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = pickerData[row]
        return row
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // array[row]["type1"] as? String
        
        let section = pickerView.tag/10
        let tableRow = pickerView.tag%10
        /*
         let  allSecArray = tableViewDataDic.allKeys as NSArray
         let sectionTitle = allSecArray.object(at: section) as! String
         let sectionArray1 = tableViewDataDic.object(forKey: String(sectionTitle)) as! NSArray
         let sectionArray : NSMutableArray = (sectionArray1.mutableCopy()) as! NSMutableArray
         var info = (sectionArray[tableRow] as! Dictionary<String, String>)
         info["value"]   = pickerData[row] as String
         sectionArray[tableRow] = info
         tableViewDataDic[sectionTitle] = sectionArray
         Helper.setUserProfileData(settinData: tableViewDataDic)
         */
        saveUserProfileInfo(section: section, row: tableRow, textValue: pickerData[row] as String)
        
        let indexPath = IndexPath(row: tableRow, section:section)
        let tableViewCell = tableView.cellForRow(at: indexPath) as! ProfileTableCell
        tableViewCell.infoValueButton.setTitle(pickerData[row] as String, for: .normal)
        
        
    }
    
    func saveUserProfileInfo(section:Int, row:Int,textValue:String) {
        
         
        let  allSecArray = tableViewDataDic.allKeys as NSArray
        let sectionTitle = allSecArray.object(at: section) as! String
        let sectionArray1 = tableViewDataDic.object(forKey: String(sectionTitle)) as! NSArray
        let sectionArray : NSMutableArray = (sectionArray1.mutableCopy()) as! NSMutableArray
        var info = (sectionArray[row] as! Dictionary<String, String>)
        info["value"]   = textValue
        sectionArray[row] = info
        tableViewDataDic[sectionTitle] = sectionArray
        Helper.setUserProfileData(settinData: tableViewDataDic)
    }
    
    
    @IBAction func pickerViewCancelButton(_ sender: Any) {
        showHidePickerViewContainer(isHidden: true)
    }
    @IBAction func pickerViewDoneButton(_ sender: Any) {
         
        if(!datePicker.isHidden){
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateStyle = .medium
            dateFormatter1.timeStyle = .none
            let dob = dateFormatter1.string(from: datePicker.date)
            self.resignFirstResponder()
            let section = datePicker.tag/10
            let row = datePicker.tag % 10
            let indexPath = IndexPath(row: section, section:row)
            self.saveUserProfileInfo(section: section, row: row, textValue: dob)
         
          //  let tableViewCell = tableView.cellForRow(at: indexPath) as! ProfileTableCell
          //  tableViewCell.infoValueButton.setTitle(dob, for: .normal)
             
        }
        showHidePickerViewContainer(isHidden: true)
        
    }
    
    
    var _inputView: UIView?
    
    override var canBecomeFirstResponder: Bool { return true }
    override var canResignFirstResponder: Bool { return true }
    
    override var inputView: UIView? {
        set { _inputView = newValue }
        get { return _inputView }
    }
    
    
}



extension UserProfileViewController: UIKeyInput {
    
    
    var hasText: Bool { return true }
    func insertText(_ text: String) {
        print(text)
        if (text == "\n") {
            self.resignFirstResponder()
        }else {
            
            if(selectedRow == 3 || selectedRow == 4){  // weight and height value
                guard let _ = Int(text)
                    else{return}
            }
            keyboardTypeingText = keyboardTypeingText + text
            let indexPath = IndexPath(row: selectedRow, section:selectedSection)
            let tableViewCell = tableView.cellForRow(at: indexPath) as! ProfileTableCell
            tableViewCell.infoValueButton.setTitle(keyboardTypeingText, for: .normal)
            self.saveUserProfileInfo(section: selectedSection, row: selectedRow, textValue: keyboardTypeingText)
        }
    }
    func deleteBackward() {
        
    }
}
