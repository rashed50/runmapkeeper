/**
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import HealthKit

class PranProfileViewController: UITableViewController,UITextFieldDelegate {
  private enum ProfileSection: Int {
    case ageSexBloodType
    case weightHeightBMI
    case readHealthKitData
    case saveBMI
    case authorizeHealthKit
  }
  
  private enum ProfileDataError: Error {
    case missingBodyMassIndex
    
    var localizedDescription: String {
      switch self {
      case .missingBodyMassIndex:
        return "Unable to calculate body mass index with available profile data."
      }
    }
  }
  
  @IBOutlet private var ageLabel: UILabel!
  @IBOutlet private var bloodTypeLabel: UILabel!
  @IBOutlet private var biologicalSexLabel: UILabel!
  @IBOutlet private var weightLabel: UILabel!
  @IBOutlet private var heightLabel: UILabel!
  @IBOutlet private var bodyMassIndexLabel: UILabel!
  
    @IBOutlet weak var authorizeHealthKitLabel: UILabel!
    
    @IBOutlet weak var loadOrCalculateBMIDataLabel: UILabel!
    
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var biologicalSexTextFiled: UITextField!
    @IBOutlet weak var bloodTextFiled: UITextField!
    
    @IBOutlet weak var weightTextFiled: UITextField!
    
    @IBOutlet weak var heightTextFiled: UITextField!
    @IBOutlet weak var bmiTextFiled: UITextField!
    
    
 //   @IBOutlet weak var myPickerView: UIPickerView!
 //   @IBOutlet weak var pickerViewContainer: UIView!
 //   var pickerData  = [String]()
    
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         updateHealthInfo()
        ageText.delegate = self
        bloodTextFiled.delegate = self
        biologicalSexTextFiled.delegate = self
        heightTextFiled.delegate = self
        weightTextFiled.delegate = self
        setNavigationBarBackButton()
        Helper.setNavigationBarProperty(navbar: self.navigationController, size: 18, title: "Profile & BMI")
        self.title = "Profile & BMI"
        if(Helper.isAlreadyAuthoriseHealthKit()){
            authorizeHealthKitLabel.text = ""
        }
    }
    
 
    
  private let userHealthProfile = UserHealthProfile()
  
  private func updateHealthInfo() {
    loadAndDisplayAgeSexAndBloodType()
    loadAndDisplayMostRecentWeight()
    loadAndDisplayMostRecentHeight()
  }
  
  private func loadAndDisplayAgeSexAndBloodType() {
    do {
      let userAgeSexAndBloodType = try ProfileDataStore.getAgeSexAndBloodType()
      userHealthProfile.age = userAgeSexAndBloodType.age
      userHealthProfile.biologicalSex = userAgeSexAndBloodType.biologicalSex
      userHealthProfile.bloodType = userAgeSexAndBloodType.bloodType
      updateLabels()
    } catch let error {
      displayAlert(for: error)
    }
  }
  
  private func updateLabels() {
    if let age = userHealthProfile.age {
      ageLabel.text = "\(age)"
    }

    if let biologicalSex = userHealthProfile.biologicalSex {
      biologicalSexLabel.text = biologicalSex.stringRepresentation
     // biologicalSexTextFiled.text =  biologicalSex.stringRepresentation
    }

    if let bloodType = userHealthProfile.bloodType {
      bloodTypeLabel.text = bloodType.stringRepresentation
    }
    
    if let weight = userHealthProfile.weightInKilograms {
      let weightFormatter = MassFormatter()
      weightFormatter.isForPersonMassUse = true
      weightLabel.text = weightFormatter.string(fromKilograms: weight)
    //  weightTextFiled.text = weightFormatter.string(fromKilograms: weight).components(separatedBy: " ").first
    }
    
    if let height = userHealthProfile.heightInMeters {
      let heightFormatter = LengthFormatter()
      heightFormatter.isForPersonHeightUse = true
        heightLabel.text = heightFormatter.string(fromMeters: height)
      //  heightTextFiled.text = String(format: "%.2f", heightFormatter.string(fromMeters: height).components(separatedBy: " ").first!)
    }
   
    if let bodyMassIndex = userHealthProfile.bodyMassIndex {
      bodyMassIndexLabel.text = String(format: "%.02f", bodyMassIndex)
   
    }
  }
  
  private func loadAndDisplayMostRecentHeight() {
    //1. Use HealthKit to create the Height Sample Type
    guard let heightSampleType = HKSampleType.quantityType(forIdentifier: .height) else {
      print("Height Sample Type is no longer available in HealthKit")
      return
    }
    
    ProfileDataStore.getMostRecentSample(for: heightSampleType) { (sample, error) in
      guard let sample = sample else {
        if let error = error {
          self.displayAlert(for: error)
        }
        
        return
      }
      
      //2. Convert the height sample to meters, save to the profile model,
      //   and update the user interface.
        let heightInMeters = sample.quantity.doubleValue(for: HKUnit.meter())  //.meter()
      self.userHealthProfile.heightInMeters = heightInMeters
        let heightInInche = sample.quantity.doubleValue(for: HKUnit.inch())
        self.userHealthProfile.heightInInche = heightInInche
      self.updateLabels()
    }
  }
  
  private func loadAndDisplayMostRecentWeight() {
    guard let weightSampleType = HKSampleType.quantityType(forIdentifier: .bodyMass) else {
      print("Body Mass Sample Type is no longer available in HealthKit")
      return
    }
    
    ProfileDataStore.getMostRecentSample(for: weightSampleType) { (sample, error) in
      guard let sample = sample else {
        if let error = error {
          self.displayAlert(for: error)
        }
        return
      }
      
      let weightInKilograms = sample.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
        self.userHealthProfile.weightInKilograms = weightInKilograms
        let weightInPound = sample.quantity.doubleValue(for: HKUnit.pound())
        self.userHealthProfile.weightInPound = weightInPound
        self.updateLabels()
    }
  }
  
  private func saveBodyMassIndexToHealthKit() {
    guard let bodyMassIndex = userHealthProfile.bodyMassIndex else {
      displayAlert(for: ProfileDataError.missingBodyMassIndex)
      return
    }
    
    ProfileDataStore.saveBodyMassIndexSample(bodyMassIndex: bodyMassIndex,
                                             date: Date())
  }
  
  private func displayAlert(for error: Error) {
    let alert = UIAlertController(title: nil,
                                  message: error.localizedDescription,
                                  preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK.",
                                  style: .default,
                                  handler: nil))
    present(alert, animated: true, completion: nil)
  }
  
  //MARK:  UITableView Delegate
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let section = ProfileSection(rawValue: indexPath.section) else {
      fatalError("A Profile Section should map to the index path's section")
    }
    
    switch section {
    case .saveBMI:
      saveBodyMassIndexToHealthKit()
    case .readHealthKitData:
      updateHealthInfo()
    default:
        
        if(Helper.isAlreadyAuthoriseHealthKit()){
            break
        }else{
            authorizeHealthKit()
            Helper.setAlreadyAuthoriseHealthKit(onOff: true)
        }
    }
  }
    
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
      //  if(textField.tag == 1){
           // loadPickerView(rowNumber: textField.tag%10, sectionNumber: textField.tag/10)
     //   }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    private func authorizeHealthKit() {
       HealthKitSetupAssistant.authorizeHealthKit { (authorized, error) in
         guard authorized else {
           let baseMessage = "HealthKit Authorization Failed"
           if let error = error {
             print("\(baseMessage). Reason: \(error.localizedDescription)")
           } else {
             print(baseMessage)
           }
           return
         }
         DispatchQueue.main.async {
           print("HealthKit Successfully Authorized. Plese Refresh View")
           self.backToMainViewController()
         }
         
       }
    }
    
    func setNavigationBarBackButton() {


            self.navigationItem.setHidesBackButton(true, animated:false)

            //your custom view for back image with custom size
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))

            if let imgBackArrow = UIImage(named: "back-arrow-png") {
                imageView.image = imgBackArrow
            }
            view.addSubview(imageView)

            let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
            view.addGestureRecognizer(backTap)

            let leftBarButtonItem = UIBarButtonItem(customView: view)
            self.navigationItem.leftBarButtonItem = leftBarButtonItem
            
            
            }
       

       @objc func backToMainViewController() {
              
               let transition = CATransition()
               transition.duration = 0.4
               transition.type = CATransitionType.push
                transition.subtype = CATransitionSubtype.fromLeft
               self.navigationController?.view.layer.add(transition, forKey: kCATransition)
               self.navigationController?.popViewController(animated: false)
       
                  
           }
    
    
    
    /*
    
    func loadPickerView(rowNumber: Int,sectionNumber:Int) {
       
           self.myPickerView.delegate = self
           self.myPickerView.dataSource = self
           self.myPickerView.tag = sectionNumber*10 + rowNumber
           if(sectionNumber == 0 && rowNumber == 0){
               pickerData = ["10%","20%","30%","40%","50%","60%","70%","80%","90%","100% (standard)"]
           }else if(sectionNumber == 0 && rowNumber == 4 ){
               pickerData = ["1 km","2 km","3 km","4 km","5 km","6 km","7 km","8 km","9 km","10 km (standard)"]
           }
           else if(sectionNumber == 2 && rowNumber == 2 ){
               pickerData = ["- vaelg herunder -","Pil","Bil","VIP >>"]
           }
           else if(sectionNumber == 2 && rowNumber == 4 ){
               pickerData = ["16x (naer)","15x","14x","13x","12x (standard)","11x","10x","9x","8x","7x (fjern)"]
           }
           else if((sectionNumber == 2 && rowNumber == 5)){
               pickerData = ["100%","90%","80%","70%","60%","50%","40%","30%","20%","10%"]
           }else if(sectionNumber == 1 && rowNumber == 3 ){
               pickerData = ["5 minutter","10 minutter","15 minutter","20 minutter (standard)","25 minutter","30 minutter","35 minutter","40 minutter","45 minutter","50 minutter","55 minutter","60 minutter"]
           }
          // showHidePickerViewContainer(isHidden: false)
           self.myPickerView.reloadAllComponents()
         DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 4)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
         }
       }
       
       func showHidePickerViewContainer(isHidden:Bool) {
           
          self.pickerViewContainer.frame.size.width = UIScreen.main.bounds.size.width
          UIView.animate(withDuration: 0.01, animations: {
           if(isHidden)
             {
              self.pickerViewContainer.frame.origin.y = 1500
              } else {
               self.pickerViewContainer.frame.origin.y = self.view.frame.size.height - 300
              }
              self.pickerViewContainer.layoutIfNeeded()
            }) { (finished) in
               UIView.animate(withDuration: 0.1, animations: {})
           }
       
       }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }

       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
          return pickerData.count
       }
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
          let row = pickerData[row]
          return row
       }
       func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
              // array[row]["type1"] as? String
         
           
       }
 
    */
}
