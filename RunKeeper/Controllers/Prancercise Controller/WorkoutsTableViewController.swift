/**
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import HealthKit

class WorkoutsTableViewController: UITableViewController {
  private enum WorkoutsSegues: String {
    case showCreateWorkout
    case finishedCreatingWorkout
  }
  
  private var workouts: [Prancercise]?
  private let prancerciseWorkoutCellID = "PrancerciseWorkoutCell"
  
  lazy var dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.timeStyle = .short
    formatter.dateStyle = .medium
    return formatter
  }()
    
    
    @IBOutlet weak var startNewWorkoutsButton: UIBarButtonItem!
    
    
    @IBAction func startNewWorkoutsAction(_ sender: Any) {
        
       
         showWorkoutExcersize()
        
    }
    
    
    func startNewWorkoutsAction() {
        
        if(!(Helper.isAlreadyAuthoriseHealthKit())){
            let alert = UIAlertController.init(title: "Permission", message: "Do you allow to Healthkit permission", preferredStyle: .alert)
            let action = UIAlertAction.init(title: "Ok", style: .default, handler:  { action -> Void in
                self.authorizeHealthKit()
                Helper.setAlreadyAuthoriseHealthKit(onOff: true)
                DispatchQueue.main.async {
                 self.showWorkoutExcersize()
                }
                
            })
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }else {
            showWorkoutExcersize()
        }
        
    }
    
    private func authorizeHealthKit() {
       HealthKitSetupAssistant.authorizeHealthKit { (authorized, error) in
         guard authorized else {
           let baseMessage = "HealthKit Authorization Failed"
           if let error = error {
             print("\(baseMessage). Reason: \(error.localizedDescription)")
           } else {
             print(baseMessage)
           }
           return
         }
         
         
       }
    }
    
    func showWorkoutExcersize() {
        
        let detailsView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "createWorkoutTableViewController") as! CreateWorkoutTableViewController
              self.navigationController?.pushViewController(detailsView, animated: true)
    }
    override func viewDidLoad() {
    super.viewDidLoad()
    clearsSelectionOnViewWillAppear = false
    setNavigationBarBackButton()
    Helper.setNavigationBarProperty(navbar: self.navigationController, size: 18, title: "Prancercise Workouts")
    self.title  = "Prancercise Workouts"
    
   
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    reloadWorkouts()
  }
  
  func reloadWorkouts() {
    print(DBHelper.getAllPrancerciseWorkoutInformation())
 //   WorkoutDataStore.loadPrancerciseWorkouts { (workouts, error) in
      self.workouts = DBHelper.getAllPrancerciseWorkoutInformation()
        //workouts
      self.tableView.reloadData()
   // }
  }
}

extension WorkoutsTableViewController {
  override func tableView(_ tableView: UITableView,
                          numberOfRowsInSection section: Int) -> Int {
    return workouts?.count ?? 0
  }
  
  override func tableView(_ tableView: UITableView,
                          cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let workouts = workouts else {
      fatalError("""
               CellForRowAtIndexPath should \
               not get called if there are no workouts
               """)
    }
     
    let cell = tableView.dequeueReusableCell(withIdentifier:
      prancerciseWorkoutCellID, for: indexPath)
     
    let workout = workouts[indexPath.row]
  
         let dateFormatter = DateFormatter()
         dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
         dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
         dateFormatter.timeZone = .current
     
        cell.textLabel?.text = dateFormatter.string(from: Date.init(seconds: workout.duration))
      
         cell.detailTextLabel?.text = String(format: "CaloriesBurned: %.2f",
                                          workout.colorie)
    
    return cell
  }
    
    
    
    
    func setNavigationBarBackButton() {


            self.navigationItem.setHidesBackButton(true, animated:false)

            //your custom view for back image with custom size
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            let imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))

            if let imgBackArrow = UIImage(named: "back-arrow-png") {
                imageView.image = imgBackArrow
            }
            view.addSubview(imageView)

            let backTap = UITapGestureRecognizer(target: self, action: #selector(backToMainViewController))
            view.addGestureRecognizer(backTap)

            let leftBarButtonItem = UIBarButtonItem(customView: view)
            self.navigationItem.leftBarButtonItem = leftBarButtonItem
            
            
            }
       

       @objc func backToMainViewController() {
              
               let transition = CATransition()
               transition.duration = 0.4
               transition.type = CATransitionType.push
               transition.subtype = CATransitionSubtype.fromLeft
               self.navigationController?.view.layer.add(transition, forKey: kCATransition)
               self.navigationController?.popViewController(animated: false)
       
                  
           }
}

