/**
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import HealthKit

class HealthKitSetupAssistant {
    
      

    /**
     * Gets a users age from a date. Only takes into account years.
     *
     * @param age The date of birth.
     * @return The age in years.
     */
    
    /*
    - (float) getAgeFromDateOfBirth:(NSDate*) dateOfBirth {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
        NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
        NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:dateOfBirth];

        if (([dateComponentsNow month] < [dateComponentsBirth month]) ||
            (([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day]))) {
            return [dateComponentsNow year] - [dateComponentsBirth year] - 1;
        } else {
            return [dateComponentsNow year] - [dateComponentsBirth year];
        }
    }

    - (float) convertKilocaloriesToMlKmin:(float)kilocalories WeigthInKg:(float)weightKgs
    {
        float kcalMin = kilocalories / 1440.0;
        kcalMin /= 5;

        return ((kcalMin / (weightKgs)) * 1000.0);
    }

    -(float)convertMetresToCentimetre:(float) metres{
        return metres * 100;
    }

    - (float) calculateDistanceTravelledInKM:(int)stepsTaken EntityStrideLength:(float) entityStrideLength
    {
        return (((float) stepsTaken * entityStrideLength) / 1000);
    }


    /**
     * Gets the MET value for an activity. Based on https://sites.google.com/site/compendiumofphysicalactivities/Activity-Categories/walking .
     *
     * @param speedInMph The speed in miles per hour
     * @return The met value.
     */

    - (float) getMetForActivity:(float) speedInMph
    {
        if (speedInMph < 2.0) {
            return 2.0f;
        } else if (speedInMph == 2.0f) {
            return 2.8f;
        } else if (speedInMph > 2.0f  && speedInMph <= 2.7f) {
            return 3.0f;
        } else if (speedInMph > 2.8f && speedInMph <= 3.3f) {
            return 3.5f;
        } else if (speedInMph > 3.4f && speedInMph <= 3.5f) {
            return 4.3f;
        } else if (speedInMph > 3.5f && speedInMph <= 4.0f) {
            return 5.0f;
        } else if (speedInMph > 4.0f && speedInMph <= 4.5f) {
            return 7.0f;
        } else if (speedInMph > 4.5f && speedInMph <= 5.0f) {
            return 8.3f;
        } else if (speedInMph > 5.0f) {
            return 9.8f;
        }
        return 0;
    }
     
     
     - (float) harrisBenedictRmrWithGender:(Gender) gender WeigthKg:(float) weightKg Age:(float) age HeightCm:(float)heightCm {
         if (gender == Felmale) {
             return 655.0955f + (1.8496f * heightCm) + (9.5634f * weightKg) - (4.6756f * age);
         } else {
             return 66.4730f + (5.0033f * heightCm) + (13.7516f * weightKg) - (6.7550f * age);
         }

     }
    
    */
    
    

    
  private enum HealthkitSetupError: Error {
    case notAvailableOnDevice
    case dataTypeNotAvailable
  }
  
  class func authorizeHealthKit(completion: @escaping (Bool, Error?) -> Void) {
    //1. Check to see if HealthKit Is Available on this device
    guard HKHealthStore.isHealthDataAvailable() else {
      completion(false, HealthkitSetupError.notAvailableOnDevice)
      return
    }
    
    //2. Prepare the data types that will interact with HealthKit
    guard
      let dateOfBirth = HKObjectType.characteristicType(forIdentifier: .dateOfBirth),
      let bloodType = HKObjectType.characteristicType(forIdentifier: .bloodType),
      let biologicalSex = HKObjectType.characteristicType(forIdentifier: .biologicalSex),
      let bodyMassIndex = HKObjectType.quantityType(forIdentifier: .bodyMassIndex),
      let height = HKObjectType.quantityType(forIdentifier: .height),
      let bodyMass = HKObjectType.quantityType(forIdentifier: .bodyMass),
      let activeEnergy = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)
      else {
        completion(false, HealthkitSetupError.dataTypeNotAvailable)
        return
    }
    
    //3. Prepare a list of types you want HealthKit to read and write
    let healthKitTypesToWrite: Set<HKSampleType> = [bodyMassIndex,
                                                    activeEnergy,
                                                    HKObjectType.workoutType()]
    let healthKitTypesToRead: Set<HKObjectType> = [dateOfBirth,
                                                   bloodType,
                                                   biologicalSex,
                                                   bodyMassIndex,
                                                   height,
                                                   bodyMass,
                                                   HKObjectType.workoutType()]
    
    //4. Request Authorization
    HKHealthStore().requestAuthorization(toShare: healthKitTypesToWrite,
                                         read: healthKitTypesToRead) { (success, error) in
                                          completion(success, error)
    }
  }
}
