//
//  DatabaseOperation.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 29/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import UIKit
import GRDB

class DBHelper: NSObject {
    static let dbPath = "\(Helper.getDocumentDirectory())/\("runkeeper.sqlite")"
    static var dbQueue = DatabaseQueue()
    
    
    static func CreateDatabase(){
        do
        {
            // let basePath = Helper.getDocumentDirectory()
            dbQueue = try DatabaseQueue(path: dbPath)
            // dbQueue.path
            try dbQueue.inDatabase
            {
                db in try db.execute(sql: "CREATE TABLE Run ( id INTEGER PRIMARY KEY AUTOINCREMENT,distance TEXT ,timestamp INT , duration INT, calorie TEXT ,locations TEXT  NULL,activitytype INT,stepcount INT)")
                try db.execute(sql: "CREATE TABLE Location ( id INTEGER PRIMARY KEY AUTOINCREMENT,runId INT ,latitude TEXT  NULL,longitude TEXT  NULL, timestamp TEXT  NULL)")
                try db.execute(sql: "CREATE TABLE Prancercise ( id INTEGER PRIMARY KEY AUTOINCREMENT,timestamp INT , duration INT, calorie float )")
                
            }
        }
        catch (let error)
        {
            print("Erreur !\(error)")
        }
    }
    static func insetRunInformation(run : Run,locationString: String){
        do {
            
            try dbQueue.write { db in
                
                try db.execute(
                    sql: "INSERT INTO Run (distance,timestamp,duration,calorie,locations,activitytype,stepcount) VALUES (?,?,?,?,?,?,?)",
                    arguments: [run.distance,run.runDate,run.duration,run.calorie,locationString,run.activityType,run.stepCount])
              
                //   try db.execute(
                //  sql: "UPDATE player SET score = :score WHERE id = :id",
                //  arguments: ["score": 1000, "id": 1])
                // }
            }
            
        } catch (let errorr) {
            print(errorr)
        }
        
    }
    
    
    static func getRunInformation(id:Int)-> Run?{
        var arun :Run?
        do{
            try dbQueue.read { db in
                if let row = try Row.fetchOne(db, sql: "SELECT * FROM Run WHERE id = ?", arguments: [id]) {
                    
                    let runId :Int = row["id"]
                    let distance: String = row["distance"]
                    let runDate: Int64 = row["timestamp"]
                    let duration: Int = row["duration"]
                    let calorie: String = row["calorie"]
                    let locations : String = row["locations"]
                    let activityType: Int8 = row["activitytype"]
                    let stepCount: Int64 = row["stepcount"]
                    
                    let locationList = Helper.jsontStringToRunLocationList(locString: locations)
                    arun =  Run.init(id: runId, distance: Double(distance)!, time: runDate, duration: duration,calorie:Float(calorie)!, runLoc: locationList,activityType:activityType,stepCount: stepCount)
                    
                }
            }
        }catch (let errorr) {
            print(errorr)
        }
        
        return arun
    }
    
    static func getAllRunInformation()-> [Run]{
        var runList = [Run]()
        do{
            try dbQueue.read { db in
                let allrow = try Row.fetchAll(db, sql: "SELECT * FROM Run")
                    for row in allrow{
                        let runId :Int = row["id"]
                        let distance: String = row["distance"]
                        let runDate: Int64 = row["timestamp"]
                        let duration: Int = row["duration"]
                        let calorie: String = row["calorie"]
                        let locations : String = row["locations"]
                        let actType: Int8 = row["activitytype"]
                        let stepCount: Int64 = row["stepcount"]
                        let locationList = Helper.jsontStringToRunLocationList(locString: locations)
                        let  arun :Run =  Run.init(id: runId, distance: Double(distance)!, time: runDate, duration: duration,calorie:Float(calorie)!, runLoc: locationList,activityType: actType,stepCount: stepCount)
                        runList.append(arun);
                        
                        
                    }
            
            }
        }catch (let errorr) {
            print(errorr)
        }
        
        return runList
    }
    static func insetRunLocationInformation(run : Run,runloc : RunLocation){
        do {
            try dbQueue.write{ db in
                try db.execute(
                    sql: "INSERT INTO Location (runId,latitude,longitude,timestamp) VALUES (?,?,?,?)",
                    arguments: [run.runId,runloc.latitude,runloc.longitude,runloc.timeStamp])
            }
        } catch (let errorr) {
            print(errorr)
        }
    }
    static func insetPrancerciseInformation(pran : Prancercise){
           do {
               try dbQueue.write{ db in
                   try db.execute(
                 //   id INTEGER PRIMARY KEY AUTOINCREMENT,timestamp INT , duration INT, calorie float
                       sql: "INSERT INTO Prancercise (timestamp,duration,calorie) VALUES (?,?,?)",
                    arguments: [pran.timeStamp,pran.duration,pran.colorie])
               }
           } catch (let errorr) {
               print(errorr)
           }
       }
    static func getAllPrancerciseWorkoutInformation()-> [Prancercise]{
        var runList = [Prancercise]()
        do{
            try dbQueue.read { db in
                let allrow = try Row.fetchAll(db, sql: "SELECT * FROM Prancercise")
                    for row in allrow{
                      //  let runId :Int = row["id"]
                       // let distance: String = row["distance"]
                        let runDate: Int64 = row["timestamp"]
                        let duration: Int64 = row["duration"]
                        let calorie: Double = row["calorie"]
                    
                        let aprancercise  =  Prancercise.init(timedate: runDate, duration: duration, colorie: calorie)
                       
                        runList.append(aprancercise);
                    }
            
            }
        }catch (let errorr) {
            print(errorr)
        }
        
        return runList
    }
    
    
    
// end of this class
}
