//
//  Helper.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-19.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit
import MessageUI

 


let APP_HEXA_COlOR_CODE = "63C7D4"
let APP_TEXT_HEXA_COlOR_CODE = "ffffff"

let  APP_NAME  = "RUN Map Keeper"



@objc
class Helper: NSObject {
   
    
    
   static func getAgeFromDOB(date: String) -> (Int) {

        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MM-yyyy"
        let dateOfBirth = dateFormater.date(from: date)

        let calender = Calendar.current

        let dateComponent = calender.dateComponents([.year, .month, .day], from:
        dateOfBirth!, to: Date())
       //(dateComponent.year!, dateComponent.month!, dateComponent.day!)
        return (dateComponent.year!)
    }
    static func getRunActivityTypeById(value:Int8)-> String{
        if(value == 1){
            return "Walking"}
        else if(value == 2){
            return "Running"}
        else{
            return "Automative"}
    }
    static func getRunActivityTypeId(value:String)-> Int8{
        if(value == "Walking"){
            return 1}
        else if(value == "Running"){
            return 2}
        else{
            return 3}
    }
    
    static   func calculateEnergyExpenditureWith(height:Float,ageCalculated:Float,weight:Float,gender:String,durationInSeconds:Int,stepsTaken:Int,strideLength:Float,kmTravelled:Float) -> Float {
           
        
       let harrisBenedictRmR =   self.convertKilocaloriesToMlKmin(kilocalories:self.harrisBenedictRmrWithGender(gender: gender, weightKg: weight, age: ageCalculated, heightCm: strideLength) , weightKgs: weight)
        
       
        let  hours  = Double(durationInSeconds) / (3600.0);
        let speedInMph = (Double(kmTravelled ) / 1000.0) / hours;
        let metValue = self.getMetForActivity(speedInMph: Float(speedInMph))

        let constant : Float = 3.5;

        let correctedMets :Float = metValue * (constant / harrisBenedictRmR);

        return correctedMets * Float( hours) * weight ;
    }
    
   private static func harrisBenedictRmrWithGender(gender:String, weightKg:Float, age:Float, heightCm:Float) -> Float {
        
        if (gender == "female") {
            return 655.0955 + (1.8496 * heightCm) + (9.5634 * weightKg) - (4.6756 * age);
        } else {
            return 66.4730 + (5.0033 * heightCm) + (13.7516 * weightKg) - (6.7550 * age);
        }

    }
    
   private  static func convertKilocaloriesToMlKmin(kilocalories:Float,weightKgs:Float) -> Float {
        
          var kcalMin = kilocalories / 1440.0;
          kcalMin  = kcalMin / 5;

          return ((kcalMin / (weightKgs)) * 1000.0);
      }
  private  static func getMetForActivity( speedInMph:Float) -> Float
    {
        if (speedInMph < 2.0) {
            return 2.0;
        } else if (speedInMph == 2.0) {
            return 2.8;
        } else if (speedInMph > 2.0  && speedInMph <= 2.7) {
            return 3.0;
        } else if (speedInMph > 2.8 && speedInMph <= 3.3) {
            return 3.5;
        } else if (speedInMph > 3.4 && speedInMph <= 3.5) {
            return 4.3;
        } else if (speedInMph > 3.5 && speedInMph <= 4.0) {
            return 5.0;
        } else if (speedInMph > 4.0 && speedInMph <= 4.5) {
            return 7.0;
        } else if (speedInMph > 4.5 && speedInMph <= 5.0) {
            return 8.3;
        } else if (speedInMph > 5.0) {
            return 9.8;
        }
        return 0;
    }
    
    
    static func convertTimeStampToDateAsString(arun: Int64) -> String {
        
        let date = Date.init(seconds: arun)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm: a"
        let str = dateFormatter.string(from: date)
        return str
    }
     
    
    static func jsontStringToRunLocationList(locString:String)->[RunLocation]{
          
          var runLocationList = [RunLocation]()
          let data = locString.data(using: .utf8)!
                 do {
                     if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                     {
                        print(jsonArray)
                      for item in jsonArray{
                        let   aRunLocation  =   RunLocation.init(lat: item["lat"] as! Double , long: item["lon"] as! Double, time:Int64(item["timestamp"] as! Double))
                          runLocationList.append(aRunLocation)
                      }
                      
                     } else {
                         print("bad json")
                     }
                 } catch let error as NSError {
                     print(error)
                 }
        return  runLocationList
          
      }
    
  static  func transformFromJSON(_ value: Any?) -> Int64? {
           if let strValue = value as? String {
               return Int64(strValue)
           }
           return value as? Int64 ?? nil
       }
    
   static func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    static func secondsToHourMinuteCombineString(seconds: Int)-> String{
        let (hour,minute,second) = Self.secondsToHoursMinutesSeconds(seconds:seconds)
        var durationString : String
        if(hour > 0)
        {
            durationString = String.init(format: "%d Hours, %d Minutes , %d Seconds", hour,minute,second)
        }else if( minute > 0)
        {
            durationString = String.init(format: "%d Minutes , %d Seconds",minute,second)
        }else {
            durationString = String.init(format: "%d Seconds",second)
        }
        return durationString
    }
     
  static  func getDocumentDirectory() -> String {
        if let documentsPathURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            return documentsPathURL.absoluteString
        }
        return ""
    }
    
    //
    // MARK EMAIL FUNCTIONS WITH DELEGATE
    //
    
    static func sendEmail(viewCon:UIViewController, fileUrl: URL?,operationType:Int) {
           
           if MFMailComposeViewController.canSendMail() {
               
               let mail = MFMailComposeViewController()
               mail.mailComposeDelegate = viewCon as? MFMailComposeViewControllerDelegate
              
               if(operationType == 0){  // action sheet action
                   let attachmentData = NSData(contentsOfFile: fileUrl!.path)
                   let fileName : String = String(fileUrl!.absoluteString.split(separator: "/").last!)
                   let mimetype =  self.MimeTypefrom(filename: fileName)
                   mail.addAttachmentData(attachmentData! as Data, mimeType: mimetype, fileName: fileName)
                   mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
               }
               else if(operationType == 1) // user feedback and suggestion
               {
                   mail.setToRecipients(["asma084050@gmail.com"])
                   mail.title = "User Feedback and Suggestion"
                   mail.setMessageBody("<p>Your Feedback and Suggestions is Our Motivation</p>", isHTML: true)
               }
               else if(operationType == 2) // Tell A Friend to share this app
               {
                   
                   mail.title = "PDF Master"
                mail.setMessageBody("https://apps.apple.com/us/app/pdf-reader-edition-for-search-read-download-online-pdf-file/id1079022338", isHTML: true)
               }
               
               viewCon.present(mail, animated: true, completion: {
              
                   
               })
           } else {
                
           }
       }

    
       static func MimeTypefrom(filename:String)-> String{
           
           var mimeType : String = ""
           if ( filename == "jpg" ) {
               mimeType =  "image/jpeg"
           } else if (filename == "png" ) {
               mimeType = "image/png";
           } else if (filename == "doc" ) {
               mimeType = "application/msword";
           } else if (filename == "ppt" ) {
               mimeType = "application/vnd.ms-powerpoint";
           } else if (filename == "html" ) {
               mimeType =  "text/html"
           } else if (filename == "pdf" ) {
               mimeType = "application/pdf";
           }
           
           return mimeType
       }
       
   //    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
   //        controller.dismiss(animated: true)
  //     }
    

       //
      // MARK: SET NAVIGATION BAR PROPERTY
      //
    
 
    
    
        @objc
       static func setNavigationBarProperty(navbar:UINavigationController!, size: Int ,title:String){
           
           let ssize  = CGFloat(size)
           let attrs = [
               NSAttributedString.Key.foregroundColor:getTextColor(),NSAttributedString.Key.font: UIFont(name:getAppTextFontName(), size:ssize)!
            ]
           navbar.navigationBar.titleTextAttributes = attrs
           navbar.navigationBar.topItem?.title = title
          // navbar.navigationBar.backgroundColor = getAppColor()
           navbar.navigationBar.barTintColor = getAppColor()
       }
       
  

       static  func getHexaStringToColor(hexa: String) -> UIColor {

                   // Convert hex string to an integer
                   let hexint = Int(self.intFromHexString(hexStr: hexa))  //
                   let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                   let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                   let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
       
                   // Create color object, specifying alpha as well
                   let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                   return color
               }
       
         @objc
         static  func getAppColor() -> UIColor {

                // Convert hex string to an integer
                let hexint = Int(self.intFromHexString(hexStr: APP_HEXA_COlOR_CODE))  //
                let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
    
                // Create color object, specifying alpha as well
                let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                return color
            }
         @objc
         static  func getTextColor() -> UIColor {

                   // Convert hex string to an integer
                   let hexint = Int(self.intFromHexString(hexStr: APP_TEXT_HEXA_COlOR_CODE))
                   let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
                   let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
                   let blue = CGFloat((hexint & 0xff) >> 0) / 255.0

                   // Create color object, specifying alpha as well
                   let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
                   return color
               }
         static func intFromHexString(hexStr: String) -> UInt32 {
                var hexInt: UInt32 = 0
                // Create scanner
                let scanner: Scanner = Scanner(string: hexStr)
                // Tell scanner to skip the # character
                scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
                // Scan hex value
                scanner.scanHexInt32(&hexInt)
                return hexInt
            }
       private static func getAppTextFontName()-> String{
           
           return "HelveticaNeue-Bold"
          //  return "MarkerFelt-Wide"
         // return  "Georgia-Bold"
       }
         
      static func getAppName()-> String{
             return APP_NAME
         }
       @objc  static func SetButtonUIProperty(button: UIButton,title:String,fontSize:Float){
             
             let fontSize = CGFloat(fontSize)
             button.backgroundColor =  getAppColor()//  C80000
             if(title != ""){
                 button.setTitle(title, for: .normal)
             }
             button.setTitleColor(getTextColor(), for: .normal)
             button.titleLabel!.font = UIFont.init(name:getAppTextFontName()  , size:fontSize)
             button.layer.cornerRadius = 10
              
         }
    
    
    
    
      static func addBannerInViewToBottom(viewController: UIViewController) {
        /*
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.adMobBannerView.rootViewController = viewController
            appDelegate.adMobBannerView.frame = CGRect(x: 0.0,
                                                       y: viewController.view.frame.height - appDelegate.adMobBannerView.frame.height,
                                                       width: viewController.view.frame.width,
                                     height: appDelegate.adMobBannerView.frame.height)
           appDelegate.adMobBannerView.isHidden = false
           viewController.view.addSubview(appDelegate.adMobBannerView)
*/
        }
         //
    // MARK: USERDEAFULT PREFERENCE DATA
    //
         @objc
         static func setAutoStartRunPlay(onOff:Bool){
             
             let defaults = UserDefaults.standard
             defaults.set(onOff, forKey: "audoPlay")
         }
         @objc
         static func getAutoStartRunPlay()->Bool{
                
              let defaults = UserDefaults.standard
      
             if (defaults.object(forKey: "audoPlay") != nil) {
                 
                 let autoOnOff : Bool =  defaults.object(forKey: "audoPlay") as! Bool
                        return autoOnOff
                }
             return true
         }
    @objc
       static func setShowCalories(onOff:Bool){
           
           let defaults = UserDefaults.standard
           defaults.set(onOff, forKey: "calorie")
       }
       @objc
       static func getShowCalories()->Bool{
              
            let defaults = UserDefaults.standard
    
           if (defaults.object(forKey: "calorie") != nil) {
               
               let autoOnOff : Bool =  defaults.object(forKey: "calorie") as! Bool
                      return autoOnOff
              }
           return true
       }
  
    static func setAlreadyAuthoriseHealthKit(onOff:Bool){
        
        let defaults = UserDefaults.standard
        defaults.set(onOff, forKey: "authorize")
    }
    
    static func isAlreadyAuthoriseHealthKit()->Bool{
           
         let defaults = UserDefaults.standard
 
        if (defaults.object(forKey: "authorize") != nil) {
            
            let autoOnOff : Bool =  defaults.object(forKey: "authorize") as! Bool
                   return autoOnOff
           }
        return false
    }
    
    static func setUserProfileData(settinData : NSMutableDictionary){
        
        let defaults = UserDefaults.standard
        defaults.set(settinData, forKey: "userProfile")
    }
    
    static func getUserProfileData() -> NSMutableDictionary{
        
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: "userProfile") != nil) {
            let appSettingData : NSDictionary =  defaults.object(forKey: "userProfile") as! NSDictionary
            return (appSettingData.mutableCopy()) as! NSMutableDictionary
        }
        else {
                     let tableViewDataDic : NSMutableDictionary = [
                         " " : [Dictionary<String, String>](),
                     ]
                     
                     tableViewDataDic[" "] = [["title":"Full Name","action":"text","value":"Not Set"],
                                                    ["title":"Brithdate(dd/MM/yyyy) ","action":"picker","value":"Not Set"],
                                                    ["title":"Gender","action":"picker","value":"Not Set"],
                                                    ["title":"Weight (Kg)","action":"text","value":"0"],
                                                    ["title":"Height (cm) ","action":"text","value":"0"]
                     ]
              return tableViewDataDic
        }
        
    }
    static func isProfileDataExist()-> Bool{
          let info : NSMutableDictionary =  self.getUserProfileData()
                  let  allSecArray = info.allKeys as NSArray
                  let sectionTitle = allSecArray.object(at: 0) as! String
                  let sectionArray1 = info.object(forKey: String(sectionTitle)) as! NSArray
                  let sectionArray : NSMutableArray = (sectionArray1.mutableCopy()) as! NSMutableArray
                  
                  let item = (sectionArray[0] as! Dictionary<String, String>)
        if(item["value"] == "Not Set" || item["value"] == " "){
            return false
        }
        else {return true}
     }
    static func getUserProfileName()-> String{
             let info : NSMutableDictionary =  self.getUserProfileData()
                     let  allSecArray = info.allKeys as NSArray
                     let sectionTitle = allSecArray.object(at: 0) as! String
                     let sectionArray1 = info.object(forKey: String(sectionTitle)) as! NSArray
                     let sectionArray : NSMutableArray = (sectionArray1.mutableCopy()) as! NSMutableArray
                     
                     let item = (sectionArray[0] as! Dictionary<String, String>)
           if(item["value"] == "Not Set" || item["value"] == " "){
            return self.getAppName()
           }
           else {return item["value"]! }
        }
   static func getUserProfileInformation() -> UserHealthProfile{
               
            let info : NSMutableDictionary =  self.getUserProfileData()
            let  allSecArray = info.allKeys as NSArray
            let sectionTitle = allSecArray.object(at: 0) as! String
            let sectionArray1 = info.object(forKey: String(sectionTitle)) as! NSArray
            let sectionArray : NSMutableArray = (sectionArray1.mutableCopy()) as! NSMutableArray
            
            var item = (sectionArray[0] as! Dictionary<String, String>)
            let name =  item["value"]!
            item = (sectionArray[1] as! Dictionary<String, String>)
            let dob =  item["value"]!
            item = (sectionArray[2] as! Dictionary<String, String>)
            let sex =  item["value"]!
            item = (sectionArray[3] as! Dictionary<String, String>)
            let weight = Double(item["value"]!)!
            item = (sectionArray[4] as! Dictionary<String, String>)
            let height = Double(item["value"]!)!
    return UserHealthProfile.init(name: name, dob: dob, sex: sex, blood: "", height: height/100, weight: weight)
                      
            }
          
           
        
    
}

