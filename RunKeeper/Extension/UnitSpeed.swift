//
//  UnitSpeed.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 29/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import Foundation


extension UnitSpeed {
  class var secondsPerMeter: UnitSpeed {
    return UnitSpeed(symbol: "sec/m", converter: UnitConverterPace(coefficient: 1))
  }
  
  class var minutesPerKilometer: UnitSpeed {
    return UnitSpeed(symbol: "min/km", converter: UnitConverterPace(coefficient: 60.0 / 1000.0))
  }
  
  class var minutesPerMile: UnitSpeed {
    return UnitSpeed(symbol: "min/mile", converter: UnitConverterPace(coefficient: 60.0 / 1609.34))
  }
    class var meterPerSecon: UnitSpeed {
      return UnitSpeed(symbol: "m/sec", converter: UnitConverterPace(coefficient: 1))
    }
    
    class var kilometerPerMinute: UnitSpeed {
      return UnitSpeed(symbol: "km/min", converter: UnitConverterPace(coefficient:  1000.0 / 60.0))
    }
    
    class var milePerMinute: UnitSpeed {
      return UnitSpeed(symbol: "mile/min", converter: UnitConverterPace(coefficient:1609.34 / 60.0 ))
    }
    
    
}
