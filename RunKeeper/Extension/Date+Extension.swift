//
//  DVLocation.swift
//  PEEMZ
//
//  Created by Moin Uddin on 12/7/16.
//  Copyright © 2016 David Ghouzi. All rights reserved.
//

import UIKit

extension Date{
    var secondsSince1970:Int64 {
        return Int64(self.timeIntervalSince1970)
    }

    init(seconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(seconds))
    }
    
    
}
