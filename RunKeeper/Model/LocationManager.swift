//
//  LocationManager.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 29/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager {
  static let shared = CLLocationManager()
  
  private init() { }
}
