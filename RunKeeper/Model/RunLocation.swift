//
//  RunLocation.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 29/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import Foundation
class RunLocation{
    var latitude : Double
    var longitude: Double
    var timeStamp : Int64
    
     init(lat: Double,long:Double,time : Int64) {
        self.latitude = lat
        self.longitude = long
        self.timeStamp = time
    }
    
}
