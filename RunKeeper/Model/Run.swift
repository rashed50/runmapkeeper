//
//  Run.swift
//  RunKeeper
//
//  Created by Rashedul Hoque on 29/6/20.
//  Copyright © 2020 Rashedul Hoque. All rights reserved.
//

import Foundation

class Run {
    

    
    var runId : Int
    var distance :Double
    var runDate : Int64
    var duration : Int
    var calorie : Float
    var activityType : Int8
    var stepCount : Int64
    var locations : [RunLocation]?
    
  
    required init(id:Int, distance:Double,time : Int64, duration : Int,calorie:Float,runLoc:[RunLocation],activityType:Int8,stepCount:Int64) {
        self.runId = id
        self.distance = distance
        self.runDate = time
        self.duration = duration
        self.calorie = calorie
        self.locations = runLoc
        self.activityType = activityType
        self.stepCount = stepCount
    }
    
}
